`use strict`;

const 
    amqp = require(`amqplib`);

const
    host     = environment.amqp.host,
    port     = environment.amqp.port,
    username = environment.amqp.username,
    passowrd = environment.amqp.passor;
    
let
    client,
    channel,
    appId   = environment.app.name,
    options = { durable: false };

const 
    connect = ()=> {
        const url = `${host}:${port}`;
        
        return new Promise((resolve, reject) => {
            amqp.connect(`${url}`)
                .then(connection => {
                    connection.createChannel()
                        .then(_channel => {
                            channel = _channel;
                            resolve(connection);
                        })
                })
                .catch(() => {
                    console.log(`Cannot connect to AMQP Broker.`)
                    setTimeout(() => {
                        process.exit();
                    }, 50);
                });
        })
    };

const
    main = {
        publish : (target, topic, payload, options) => {
            if(payload.constructor === String) {
                payload = JSON.parse(payload);
            }

            const data = {
                headers   : payload.headers,
                auth      : payload.auth,
                params    : payload.params ? payload.params : payload,
                body      : payload.body,
                queue     : payload.queue,
                requestid : payload.requestid,
                source    : payload.source ? payload.source : appId,
                topic     : topic,
            };

            channel.sendToQueue(target, Buffer.from(JSON.stringify(data)));
        },
        on : (type, callback) => {
            if(type === `message`) {
                main.onMessage(callback);
                return;
            }
    
            client.on(type, callback);
        },
    
        onMessage : (callback) => {
            let 
                queue = channel.assertQueue(appId, options)
                if(queue){
                    queue
                        .then(() => {
                            channel.consume(
                                            appId, 
                                            (message)=>{
                                                callback(JSON.parse(message.content.toString()))
                                            },{ 
                                                noAck  : true 
                                            });
                        })
                }
        }
    }

module.exports = async () => {
    return new Promise(async (resolve, reject) => {
        client = await connect();
        global.amqp = main
        resolve({ ... client });
    })
    
};