`use strict`;

const { promisify } = require('util');

module.exports = { 
    get: (key) => {
        let data_ = promisify(global.cache.get).bind(global.cache)
        return data_(key);
    },
    set: (key, data, expired = 24) => {
        let data_ = promisify(global.cache.set).bind(global.cache)
        return data_(key, typeof data === 'object' ? JSON.stringify(data) : data);
    },
    delete: (key) => {
        let data_ = promisify(global.cache.del).bind(global.cache);
        return data_(key);
    }
}