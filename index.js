`use strict`;

const 
    { service, amqp, mqtt, subscriber, database, router} = require(`./app_module`),
    express   = require(`express`),
    socket = require('./src/module/socket');
    

let start = () => {
    return new Promise(async (resolve, reject) => {
        resolve({
            service : await service(),
            amqp    : await amqp(),
            //router  : await router()
        })
     }).then(async (data) => {
         const 
            app             = express();
            http            = require('http').Server(app);
            global.io       = require('socket.io')(http);
            global.ioconn   = {};
            http.listen(environment.app.port, function(){
                console.log(`HTTP server started on port ${environment.app.port}`);
            });
            socket();
            subscriber();
     });
};

start()