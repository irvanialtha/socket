`use strict`;

const route = (socket) => {
    socket.on('realtime/device', (data) => {
        control.realtime.device(socket, data);
    });
    socket.on('realtime/devicesummary', (data) => {
        control.realtime.devicesummary(socket, data);
    });
    socket.on('realtime/closedevice', (data) => {
        control.realtime.closedevice(socket, data);
    });
    
}

module.exports = () => {
    io.on('connection', (socket) => {
        route(socket);
    });
};