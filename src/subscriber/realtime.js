`use strict`;

module.exports = {
    device:(data) =>{
        let 
            socketid = data.params.socketid,
            params   = data.params.properties,
            output   = {
                devicename  : data.params.deviceName,
                total       : data.params.total,
                param       : [],
                value       : {}
            };
            for (let i in params) {
                if (typeof params[i] == 'number') {
                    output.param.push(i);
                    output.value[i] = params[i]
                }
            }
        io.to(socketid).emit("realtime/device", output)
    },
    devicesummary:(data) =>{
        let 
            socketid = data.params.socketid,
            params   = data.params.properties,
            output   = {
                devicename  : data.params.deviceName,
                param       : [],
                value       : {}
            };
            for (let i in params) {
                if (typeof params[i] == 'number') {
                    output.param.push(i);
                    output.value[i] = params[i]
                }
            }
        io.to(socketid).emit("realtime/devicesummary", output)
    },
   
}