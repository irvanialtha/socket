`use strict`;
const
    authorization = require(`${basedir}/src/middleware/authorization`);

module.exports = {
    authorization:(req, res, data)=>{
        authorization.next(req, res, req.next, data);
    },

    output:(req, res, data)=>{
        res.send(data.body)
    }
}