`use strict`;

module.exports = {
    device : (socket, data) => {
        data.socketid = socket.id
        amqp.publish(service.broker,'realtime.device',data);
    },
    devicesummary: (socket, data) => {
        data.socketid = socket.id
        amqp.publish(service.broker,'realtime.devicesummary',data);
    },
    closedevice: (socket, data) => {
        data.socketid = socket.id
        amqp.publish(service.broker,'realtime.closedevice',data);
    }
};